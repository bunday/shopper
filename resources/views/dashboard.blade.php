@extends('wrapper')
@section('content')
    <!-- Start Breadcrumb -->
    <div id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <a href="/">Home</a> <span class="separator">&gt;</span> <span> Dashboard</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->
    <!-- Start Banner -->
    <div id="banner-area" class="gradient-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-inner blog-banner">
                        <a class="btn-lucian" href="#">{{Auth::user()->name}} - Your Available Products</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->
    <br>
    <button class="btn btn-success pull-right" data-toggle="modal" data-target="#newProduct"><i class="fa fa-plus"></i>Add New Product</button>
    
    <!-- Start Main Content -->
    <div id="main-content-area" class="padtop80 padbot75">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    @if($products->count()<1)
                    <h2 class="text-center text-info">No Products in your store yet... Start by adding from the top right hand corner<h2>
                    @else
                        <table class="cart-table table-condensed ">
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">Image</th>
                                    <th class="product-name">Product Name</th>
                                    <th class="product-price">Start Price</th>
                                    <th class="product-price">Final Price</th>
                                    <th class="product-timeline">Added Since</th>
                                    <th class="product-edit">Edit</th>
                                    <th class="product-remove">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td class="product-thumbnail">
                                        <a href="#"><img alt="" height="100" width="100" src="{{$product->image}}">
                                        </a>
                                    </td>
                                    <td class="product-name"><a href="#">{{$product->name}}</a><span>Size: {{$product->size}}</span> <span>Color: {{$product->color}}</span>
                                    </td>
                                    <td class="product-price">${{$product->startPrice}}</td>
                                    <td class="product-price">${{$product->lastPrice}}</td>
                                    <td class="product-timeline">{{$product->created_at->diffForHumans()}}</td>
                                    <td class="product-edit"><button name="{{$product->name}}" siz="{{$product->size}}" col="{{$product->color}}" start="{{$product->startPrice}}" last="{{$product->lastPrice}}" id="{{$product->id}}" data-toggle="modal" data-target="#newProduct" class="fa fa-2x fa-edit btn btn-warning" onclick="editProduct(this)"></button>
                                    </td>
                                    <td class="product-remove">
                                        <form method="POST" action="/product/delete">
                                        @csrf
                                        <input type="hidden" name="productId" value="{{$product->id}}">
                                        <button type="submit" class="fa fa-trash btn btn-danger"></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    @endif
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- End Main Content -->
@endsection