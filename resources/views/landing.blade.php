
<!doctype html>
<html class="no-js" lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Title -->
        <title>{{ config('app.name', 'Shopper') }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

        <!-- Google fonts -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,400italic,500italic,600italic,600,700,700italic,900,300' rel='stylesheet' type='text/css'>	
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

		<!-- All CSS Here -->
		<!-- Bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="css/bootstrap.min.css">	
		<!-- nivo slider CSS -->
		<link rel="stylesheet" href="custom-slider/css/nivo-slider.css" type="text/css" />
		<link rel="stylesheet" href="custom-slider/css/preview.css" type="text/css" media="screen" />	
		<!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- jquery-ui.min css -->
        <link rel="stylesheet" href="css/jquery-ui.min.css">
		<!-- meanmenu css -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
		<!-- owl.carousel css --> 
        <link rel="stylesheet" href="css/owl.carousel.css">
		<!-- font-awesome css -->
        <link rel="stylesheet" href="css/font-awesome.min.css">	
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="css/colors.css">
		<!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr css -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body class="home1">

	<!-- Start Header -->
    <header id="header">
        <!-- Start Header Top -->
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="hidden-sm hidden-xs col-md-6"> 
                        <!-- Topbar Menu -->
                        <div class="topbar-menu"> 
                            <ul>
                                <li><a href="callto:+"><i aria-hidden="true" class="fa fa-phone-square"></i>+01 43973-833508</a></li>
                                <li><a href="mailto:admin@shopper.com"><i class="fa fa-envelope-square" aria-hidden="true"></i>admin@shopper.com</a></li>
                            </ul>
                        </div>
                        <!-- End Topbar Menu -->
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-4">
                        <!-- Topbar Menu -->
                        <div class="topbar-menu"> 
                           <ul>
                                <li class="drop">
                                    <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs" aria-hidden="true"></i>Account</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Check Out</a></li>
                                        <li><a href="#">Cart</a></li>	
                                    @guest 
                                        <li><a href="/login">Login</a></li>  
                                        <li><a href="/register">Register</a></li> 
                                    @else
 
                                        <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                        </li>                                                                        
                                    @endguest
									
                                    </ul>
                                </li>	
                                <li class="drop">
                                    <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe" aria-hidden="true"></i> Wishlist : <span>0 items</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Wish List Item</a></li>
                                        <li><a href="#">Wish List Item</a></li>
                                    </ul>
                                </li>		
                           </ul>
                        </div>
                        <!-- End Topbar Menu -->
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-2">
                        <!-- Start Social Icons -->
                        <div class="social-icons top-sicons"> 
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                            </ul>
                        </div>
                        <!-- End Social Icons -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->
        <!-- Start Header Bottom -->
        <div class="header-bottom-wrapper">
            <div class="header-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="hidden-xs col-sm-4 col-md-3">
                            <!-- Start Search Form -->
                            <div class="search-box-area header-search"> 
                                <form action="/product-filter" method="POST">
                                @csrf
                                    <input type="search" placeholder="Insert keyword and hit Enter" name="productkeyword" class="cat-search-box">
                                    <i class="fa fa-search"></i>
                                </form>					    
                            </div>
                            <!-- End Search Form -->
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-6"> 
                            <div class="logo-area">
                                <!-- Start Logo -->
                                <div class="logo">
                                    <a href="#">
                                        <h2 class="text-center" style="color:white">SHOPPER</h2>
                                    </a>
                                </div>
                                <!-- End Logo -->
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-4 col-md-3"> 
                            <!-- Start Cart Area -->
                            <div class="cart-inner header-cart">
                                <a class="backet-area"> 
                                    <span class="added-total">0</span>
                                    shopping cart
                                </a>	
                                
                            </div>					   
                            <!-- End Cart Area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Bottom -->			
    </header>
    <!--End Start Header -->

    <!-- Start Slider Wrap -->
    <section id="slider-wrap">
        <!-- Start Slider Area-->
        <div class="slider-area">
            <div class="bend niceties preview-1">
                <div id="nivoslider-lucian" class="slides">	
                    <img src="custom-slider/img/home-1/slide-1.jpg" alt="" title="#slider-direction-1"  />
                    <img src="custom-slider/img/home-1/slide-2.jpg" alt="" title="#slider-direction-2"  />
                </div>
                <!-- direction 1 -->
                <div id="slider-direction-1" class="slider-direction">
                    <div class="slider-content t-cn s-tb">
                        <div class="s-tb-c">
                            <h2 class="title1"><span>Best</span> Shoes</h2>
                            <h3 class="title3 hidden-xs">Different Stores, All from here</h3>
                            <div class="slider-btns"> 
                                <a href="#" class="slider-btn">Shop Now</a>
                                <a href="#" class="slider-btn2">Explore</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- direction 2 -->
                <div id="slider-direction-2" class="slider-direction">
                    <div class="slider-content t-cn s-tb">
                        <div class="s-tb-c">
                            <h2 class="title1">Any Shoe, London Style</h2>
                            <h3 class="title3 hidden-xs">Sale up to 50% off for summer days</h3>
                            <div class="slider-btns"> 
                                <a href="#" class="slider-btn">Start Shopping</a>
                                <a href="#" class="slider-btn2">Browse</a>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>
        </div>
        <!-- End slider Area-->
    </section>
    <!-- End Slider Wrap -->

    <!-- Start New Products -->
    <section id="new-products-area" class="padtop50 padbot15">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="section-title text-center title-box2">
                        <h2>What's New</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="new-product-carousel" class="owl-controls-2">
                    @foreach($new as $product)
                    <div class="col-md-3">
                        <!-- Start Single Prodcut -->
                        <div class="single-product">
                            <!-- Start Product Thumbnail -->
                            <div class="product-thumb-area">
                                <!-- Start Product Image -->
                                <div class="product-thumb">
                                    <a href="#"><img src="{{$product->image}}" width="293" height="355" alt="product" /></a>
                                    <span class="product-Sale">{{$product->user->name}}</span>
                                </div>
                                <!-- End Product Image -->
                                <!-- Start Product Hidden Info -->
                                <div class="product-hidden-info">
                                    <!-- Quick View -->
                                    <div class="quick-view">
                                        <a href="#" class="modal-view detail-link quickview" data-toggle="modal" data-target="#productModal"><i class="fa fa-eye"></i> Quick View</a>
                                    </div>
                                    <!-- End Quick View -->
                                    <!-- Start Wish List  -->
                                    <div class="wish-list-area"> 
                                        <a href="#" class="wish-list"><i aria-hidden="true" class="fa fa-heart-o"></i> Wish List</a>
                                        <a href="#" class="compare"><i aria-hidden="true" class="fa fa-exchange"></i> Compare</a>
                                    </div>
                                    <!-- End Wish List  -->
                                </div>
                                <!-- End Product Hidden Info -->
                            </div>
                            <!-- End Product Thumbnail -->
                            <!-- Start Product Info -->
                            <div class="product-short-info"> 
                                <!-- Start product short description -->
                                <p class="p-short-des"><a href="#">{{$product->name}}</a></p>
                                <!-- End product short description -->
                                <!-- Start Star Rating -->
                                <div class="star-rating"> 
                                    <ul>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star"><i class="fa fa-star" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                                <!-- End Star Rating -->
                            </div>
                            <!-- End Product Info -->
                            <!-- Start Prodcut Price Area -->
                            <div class="product-price-area"> 
                                <span class="price">
                                    <span class="amount">${{$product->lastPrice}}</span> <span><del>${{$product->startPrice}}</del></span>
                                </span>
                                <span class="add-to-cart"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i>Add to Cart</a></span>
                            </div>
                            <!-- End Prodcut Price Area -->
                        </div>
                        <!-- End Single Prodcut -->
                    </div>
                    @endforeach
                </div>
                <!-- End New prodcut carousel -->
            </div>
        </div>
    </section>
    <!-- End New Products -->

    <!-- Start Quick Category Area -->
    <div id="quick-category-area" class="padtop20 padbot25">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"> 
                    <!-- Start Single Category Item -->
                    <div class="single-category-item">  
                        <div class="category-thumb">
                            <img src="img/category-thumbs/4.png" alt="" />
                            <a href="#" class="btn-lucian">Shoes</a>
                        </div>
                    </div>
                    <!-- End Single Category Item -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4"> 
                    <!-- Start Single Category Item -->
                    <div class="single-category-item">  
                        <div class="category-thumb">
                            <img src="img/category-thumbs/5.png" alt="" />
                            <a href="#" class="btn-lucian">Men</a>
                        </div>
                    </div>
                    <!-- End Single Category Item -->
                </div>
                <div class="clearfix hidden-sm"></div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4"> 
                    <!-- Start Single Category Item -->
                    <div class="single-category-item">  
                        <div class="category-thumb">
                            <img src="img/category-thumbs/6.png" alt="" />
                            <a href="#" class="btn-lucian">Bags</a>
                        </div>
                    </div>
                    <!-- End Single Category Item -->
                </div>	
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"> 
                    <!-- Start Single Category Item -->
                    <div class="single-category-item">  
                        <div class="category-thumb">
                            <img src="img/category-thumbs/7.png" alt="" />
                            <a href="#" class="btn-lucian">Bags</a>
                        </div>
                    </div>
                    <!-- End Single Category Item -->
                </div>	
            </div>
        </div>
    </div>
    <!-- End Quick Category Area -->

    <!-- Start Best Seller -->
    <section id="best-seller-area" class="padtop30 padbot35">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <!-- Start Section Title -->
                    <div class="section-title text-center title-box2">
                        <h2>Best Seller</h2>
                    </div>
                    <!-- End Section Title -->
                </div>
            </div>
            <div class="row">
                <!-- Start Best seller carousel -->
                <div id="best-seller-carousel" class="owl-controls-2">				 
                    @foreach($random as $product)
                    <div class="col-md-3">
                        <!-- Start Single Prodcut -->
                        <div class="single-product">
                            <!-- Start Product Thumbnail -->
                            <div class="product-thumb-area">
                                <!-- Start Product Image -->
                                <div class="product-thumb">
                                    <a href="#"><img src="{{$product->image}}" width="293" height="355" alt="product" /></a>
                                    <span class="product-Sale">{{$product->user->name}}</span>
                                </div>
                                <!-- End Product Image -->
                                <!-- Start Product Hidden Info -->
                                <div class="product-hidden-info">
                                    <!-- Quick View -->
                                    <div class="quick-view">
                                        <a href="#" class="modal-view detail-link quickview" data-toggle="modal" data-target="#productModal"><i class="fa fa-eye"></i> Quick View</a>
                                    </div>
                                    <!-- End Quick View -->
                                    <!-- Start Wish List  -->
                                    <div class="wish-list-area"> 
                                        <a href="#" class="wish-list"><i aria-hidden="true" class="fa fa-heart-o"></i> Wish List</a>
                                        <a href="#" class="compare"><i aria-hidden="true" class="fa fa-exchange"></i> Compare</a>
                                    </div>
                                    <!-- End Wish List  -->
                                </div>
                                <!-- End Product Hidden Info -->
                            </div>
                            <!-- End Product Thumbnail -->
                            <!-- Start Product Info -->
                            <div class="product-short-info"> 
                                <!-- Start product short description -->
                                <p class="p-short-des"><a href="#">{{$product->name}}</a></p>
                                <!-- End product short description -->
                                <!-- Start Star Rating -->
                                <div class="star-rating"> 
                                    <ul>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star"><i class="fa fa-star" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                                <!-- End Star Rating -->
                            </div>
                            <!-- End Product Info -->
                            <!-- Start Prodcut Price Area -->
                            <div class="product-price-area"> 
                                <span class="price">
                                    <span class="amount">${{$product->lastPrice}}</span> <span><del>${{$product->startPrice}}</del></span>
                                </span>
                                <span class="add-to-cart"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i>Add to Cart</a></span>
                            </div>
                            <!-- End Prodcut Price Area -->
                        </div>
                        <!-- End Single Prodcut -->
                    </div>
                    @endforeach		
                </div>
                <!-- End Best seller carousel -->
            </div>
        </div>
    </section>
    <!-- End Best Seller -->	

    <!-- Start Footer Area -->
    <footer id="footer-area">
        <!-- Start Footer Top Area -->
        <div class="footer-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget wedget-contact"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Contact us</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li class="phone"><a href="callto:0(1234)567890">0 <span>(1234)</span> 567 890</a></li>
                                <li class="info-email"><a href="mailto:info@shopper.com">info@shopper.com</a></li>
                                <li class="location">Address : somewhere anywhere everywhere</li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">MY ACCOUNT</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Your Account</a></li>
                                <li><a href="#">Advanced Search</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Payment & Shipping</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Shipping Guide</a></li>
                                <li><a href="#">Locations We Ship To</a></li>
                                <li><a href="#">Estimated Delivery Time	</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>	
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Customer Service</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Shipping Policy</a></li>
                                <li><a href="#">Compensation First</a></li>
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">Return Policy</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>	
                </div>
            </div>
        </div>
        <!-- End Footer Top Area -->
        <!-- Start Footer Bottom -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6"> 
                        <!-- Start Copyright Text -->
                        <div class="copy-right-text"> 
                            <p>Copyright &copy; 2018 <a href="#">Ife Ojemakinde.</a> All Rights Reserved</p>
                        </div>
                        <!-- End Copyright Text -->
                    </div>
                    <div class="col-md-6">
                        <!-- Start Cards -->
                        <div class="card-buttons"> 
                            <img src="img/cart/cards.png" alt="" />
                        </div>
                        <!-- End Cards -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </footer>
	<!-- End Footer Area -->

	<!-- Start Quickview Product -->
	<div id="quickview-wrapper">
		<!-- Modal -->
		<div class="modal fade" id="productModal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<div class="modal-product">
							<!-- Start product images -->
							<div class="product-images">
								<div class="main-image images">
									<img alt="" src="img/products/1.jpg">
								</div>
							</div>
							<!-- end product images -->
							<div class="product-info">
								<h1>Sample text product</h1>
								<div class="price-box-3">
									<div class="s-price-box">
										<span class="new-price">$17.20</span>
										<span class="old-price">$45.00</span>
									</div>
								</div>
								<a href="shop.html" class="see-all">See all features</a>
								<div class="quick-add-to-cart">
									<div class="cart-plus-minus">
										<input type="text" class="cart-plus-minus-box" name="qtybutton" value="1">
									</div>
									<a href="#" class="qucik-addtocart"><i aria-hidden="true" class="fa fa-plus"></i> Add to cart</a>
								</div>
								<div class="quick-desc">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero.
								</div>
								<div class="social-sharing">
									<div class="widget widget_socialsharing_widget">
										<h3 class="widget-title-modal">Share this product</h3>
										<ul class="social-icons">
											<li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
											<li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
											<li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
											<li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
											<li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div><!-- .product-info -->
						</div><!-- .modal-product -->
					</div><!-- .modal-body -->
				</div><!-- .modal-content -->
			</div><!-- .modal-dialog -->
		</div>
		<!-- END Modal -->
	</div>
	<!-- End Quickview Product -->	

	<!-- all js here -->
	<!-- jquery latest version -->
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<!-- bootstrap js -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Nivo slider js --> 		
	<script src="custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
	<script src="custom-slider/home.js" type="text/javascript"></script>	
	<!-- owl.carousel js -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- meanmenu js -->
	<script src="js/jquery.meanmenu.js"></script>
	<!-- jquery-ui js -->
	<script src="js/jquery-ui.min.js"></script>	
	<!-- wow js -->
	<script src="js/wow.min.js"></script>
	<!-- plugins js -->
	<script src="js/plugins.js"></script>
	<!-- main js -->
	<script src="js/main.js"></script>	
    </body>
</html>