
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Title -->
    <title>Your Personal Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- Google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,400italic,500italic,600italic,600,700,700italic,900,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <!-- All CSS Here -->
    <!-- Bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="custom-slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="custom-slider/css/preview.css" type="text/css" media="screen" />
    <!-- animate css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/colors.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr css -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!-- Start Header -->
    <header id="header">
        <!-- Start Header Top -->
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="hidden-sm hidden-xs col-md-6"> 
                        <!-- Topbar Menu -->
                        <div class="topbar-menu"> 
                            <ul>
                                <li><a href="callto:+"><i aria-hidden="true" class="fa fa-phone-square"></i>+01 43973-833508</a></li>
                                <li><a href="mailto:admin@shopper.com"><i class="fa fa-envelope-square" aria-hidden="true"></i>admin@shopper.com</a></li>
                            </ul>
                        </div>
                        <!-- End Topbar Menu -->
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-4">
                        <!-- Topbar Menu -->
                        <div class="topbar-menu"> 
                           <ul>
                                <li class="drop">
                                    <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs" aria-hidden="true"></i>Account</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Check Out</a></li>
                                        <li><a href="#">Cart</a></li>	
                                    @guest 
                                        <li><a href="/login">Login</a></li>  
                                        <li><a href="/register">Register</a></li> 
                                    @else
 
                                        <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                        </li>                                                                        
                                    @endguest
									
                                    </ul>
                                </li>	
                                <li class="drop">
                                    <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe" aria-hidden="true"></i> Wishlist : <span>0 items</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Wish List Item</a></li>
                                        <li><a href="#">Wish List Item</a></li>
                                    </ul>
                                </li>		
                           </ul>
                        </div>
                        <!-- End Topbar Menu -->
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-2">
                        <!-- Start Social Icons -->
                        <div class="social-icons top-sicons"> 
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss-square"></i></a></li>
                            </ul>
                        </div>
                        <!-- End Social Icons -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->
        <!-- Start Header Bottom -->
        <div class="header-bottom-wrapper">
            <div class="header-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="hidden-xs col-sm-4 col-md-3">
                            <!-- Start Search Form -->
                            <div class="search-box-area header-search"> 
                                <form action="/product-filter" method="POST">
                                @csrf
                                    <input type="search" placeholder="Insert keyword and hit Enter" name="productkeyword" class="cat-search-box">
                                    <i class="fa fa-search"></i>
                                </form>					    
                            </div>
                            <!-- End Search Form -->
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-6"> 
                            <div class="logo-area">
                                <!-- Start Logo -->
                                <div class="logo">
                                    <a href="#">
                                        <h2 class="text-center" style="color:white">SHOPPER</h2>
                                    </a>
                                </div>
                                <!-- End Logo -->
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-4 col-md-3"> 
                            <!-- Start Cart Area -->
                            <div class="cart-inner header-cart">
                                <a class="backet-area"> 
                                    <span class="added-total">0</span>
                                    shopping cart
                                </a>	
                                
                            </div>					   
                            <!-- End Cart Area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Bottom -->			
    </header>
    <!--End Start Header -->
    @yield('content')
    <script>
        function editProduct(btn){
            document.getElementById("productName").value = btn.attributes[0].value
            document.getElementById("productSize").value = btn.attributes[1].value
            document.getElementById("productColor").value = btn.attributes[2].value
            document.getElementById("productStartPrice").value = btn.attributes[3].value
            document.getElementById("productLastPrice").value = btn.attributes[4].value
            document.getElementById("productId").value = btn.attributes[5].value
            document.getElementById("productImage").required = false
            submit = document.getElementById('submitButton')
            submit.className = "btn btn-warning"
            submit.innerHTML = "Update Product Information"
            document.getElementById('productForm').action="/product/update"
        }
    </script>
    
    <div class="modal fade" id="newProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add New Product</h4>
                </div>
                <div class="modal-body">
                    <section class="boxs">
                            <div class="boxs-body">
                                <form id="productForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/product/add">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productName" class="col-sm-2 control-label">Product Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="productName" required name="productName" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productImage" class="col-sm-2 control-label">Product Image</label>
                                                <div class="col-sm-10">
                                                    <input type="file" id="productImage" name="productImage" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productSize" class="col-sm-2 control-label">Product Size</label>
                                                <div class="col-sm-10">
                                                    <input type="number" id="productSize" name="productSize" min="0" max="60" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productColor" class="col-sm-2 control-label">Product Color</label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="productColor" name="productColor" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productStartPrice" class="col-sm-4 control-label">Product Start Price (in Dollars)</label>
                                                <div class="col-sm-8">
                                                    <input type="number" id="productStartPrice" name="productStartPrice" min="0" step="0.01" max="2000" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="productLastPrice" class="col-sm-4 control-label">Product Last Price (in Dollars)</label>
                                                <div class="col-sm-8">
                                                    <input type="number" id="productLastPrice" name="productLastPrice" min="0" step="0.01" max="2000" required class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="productId" id="productId">
                                    </div>
                                           
                            </div>
                        </section>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitButton" class="btn btn-raised btn-success">Add New Product</button>
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Cancel</button>
                </div>

                                </form>
            </div>
        </div>
    </div>
    <!-- Start Footer Area -->
    <footer id="footer-area">
        <!-- Start Footer Top Area -->
        <div class="footer-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget wedget-contact"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Contact us</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li class="phone"><a href="callto:0(1234)567890">0 <span>(1234)</span> 567 890</a></li>
                                <li class="info-email"><a href="mailto:info@shopper.com">info@shopper.com</a></li>
                                <li class="location">Address : somewhere anywhere everywhere</li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">MY ACCOUNT</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Your Account</a></li>
                                <li><a href="#">Advanced Search</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Payment & Shipping</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Payment Methods</a></li>
                                <li><a href="#">Shipping Guide</a></li>
                                <li><a href="#">Locations We Ship To</a></li>
                                <li><a href="#">Estimated Delivery Time	</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>	
                    <div class="col-xm-12 col-sm-6 col-md-3"> 
                        <!-- Start Wedget -->
                        <div class="footer-wedget"> 
                            <!-- Start Wedget Title -->
                            <h4 class="wedget-title">Customer Service</h4>
                            <!-- End Wedget Title -->
                            <!-- Start Wedget Lists -->
                            <ul>
                                <li><a href="#">Shipping Policy</a></li>
                                <li><a href="#">Compensation First</a></li>
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">Return Policy</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                            <!-- End Wedget Lists -->
                        </div>
                        <!-- End Wedget -->
                    </div>	
                </div>
            </div>
        </div>
        <!-- End Footer Top Area -->
        <!-- Start Footer Bottom -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6"> 
                        <!-- Start Copyright Text -->
                        <div class="copy-right-text"> 
                            <p>Copyright &copy; 2018 <a href="#">Ife Ojemakinde.</a> All Rights Reserved</p>
                        </div>
                        <!-- End Copyright Text -->
                    </div>
                    <div class="col-md-6">
                        <!-- Start Cards -->
                        <div class="card-buttons"> 
                            <img src="img/cart/cards.png" alt="" />
                        </div>
                        <!-- End Cards -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </footer>
	<!-- End Footer Area -->
    <!-- all js here -->
    <!-- jquery latest version -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Nivo slider js -->
    <script src="custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="custom-slider/home.js" type="text/javascript"></script>
    <!-- owl.carousel js -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- meanmenu js -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- jquery-ui js -->
    <script src="js/jquery-ui.min.js"></script>
    <!-- wow js -->
    <script src="js/wow.min.js"></script>
    <!-- plugins js -->
    <script src="js/plugins.js"></script>
    <!-- main js -->
    <script src="js/main.js"></script>
</body>
</html>