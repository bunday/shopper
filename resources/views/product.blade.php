@extends('wrapper')
@section('content')
    <!-- Start Breadcrumb -->
    <div id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="breadcrumbs">
                        <a href="/">Home</a> <span class="separator">&gt;</span> <span> Product Gallery</span>
                    </div>					   
                </div>
            </div>
        </div>
    </div>
    <!-- Start Breadcrumb -->
    <!-- Start Banner -->
    <div id="banner-area" class="gradient-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <div class="banner-inner blog-banner"> 
                    <a class="btn-lucian" href="#">Products Gallery</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- End Banner -->
    <!-- Start Main Content -->
    <div id="main-content-area" class="padtop85 padbot65">
        <div class="container">
            <div class="row">
                <div class="prodcut-toolbar"> 
                    <div class="col-sm-8 col-md-8">
                        <div class="toolbar-form">
                            <form action="#" class="option-select">
                                <select class="options-inner">
                                    <option value="volvo">Sort by: Default sorting</option>
                                    <option value="mercedes">Sort by average rating</option>
                                    <option value="audi">Sort by newness</option>
                                    <option value="audi">Sort by price: low to high</option>
                                    <option value="audi">Sort by price: high to low</option>
                                </select> 
                            </form>
                            <form action="#" class="option-select">
                                <select class="options-inner">
                                    <option value="12">Show: 12 items per page</option>
                                    <option value="16"> 16 items per page</option>
                                    <option value="20"> 20 items per page</option>
                                    <option value="24"> 24 items per page</option>
                                </select> 
                            </form>								
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <!-- Nav tabs -->
                        <ul role="tablist" class="tab-icons pull-right">
                            <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="grid" title="Grid" href="#grid" aria-expanded="true"><i class="fa fa-th-large"></i></a></li>
                            <li role="presentation" class=""><a data-toggle="tab" role="tab" aria-controls="list" title="List" href="#list" aria-expanded="false"><i class="fa fa-th-list"></i></a></li>
                        </ul>					
                    </div>				
                </div>
            </div>
            <div class="row"> 
                <!-- Start Tab Content -->
                <div class="tab-content"> 
                @if($products->count()<1)
                <h4 class="text-center text-info">Oops! No product matched your search term. Consider searching again with another keyword or click <a href="/explore">HERE</a> to explore available products.<h4>
                @else
                    <!-- Start Grid products -->
                    <div id="grid" class="tab-pane active in" role="tabpanel"> 
                        <div class="products-gridview-inner"> 
                        @foreach($products as $product)
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <!-- Start Single Prodcut -->
                                <div class="single-product">
                            <!-- Start Product Thumbnail -->
                            <div class="product-thumb-area">
                                <!-- Start Product Image -->
                                <div class="product-thumb">
                                    <a href="#"><img src="{{$product->image}}" width="293" height="355" alt="product" /></a>
                                    <span class="product-Sale">{{$product->user->name}}</span>
                                </div>
                                <!-- End Product Image -->
                                <!-- Start Product Hidden Info -->
                                <div class="product-hidden-info">
                                    <!-- Quick View -->
                                    <div class="quick-view">
                                        <a href="#" class="modal-view detail-link quickview" data-toggle="modal" data-target="#productModal"><i class="fa fa-eye"></i> Quick View</a>
                                    </div>
                                    <!-- End Quick View -->
                                    <!-- Start Wish List  -->
                                    <div class="wish-list-area"> 
                                        <a href="#" class="wish-list"><i aria-hidden="true" class="fa fa-heart-o"></i> Wish List</a>
                                        <a href="#" class="compare"><i aria-hidden="true" class="fa fa-exchange"></i> Compare</a>
                                    </div>
                                    <!-- End Wish List  -->
                                </div>
                                <!-- End Product Hidden Info -->
                            </div>
                            <!-- End Product Thumbnail -->
                            <!-- Start Product Info -->
                            <div class="product-short-info"> 
                                <!-- Start product short description -->
                                <p class="p-short-des"><a href="#">{{$product->name}}</a></p>
                                <!-- End product short description -->
                                <!-- Start Star Rating -->
                                <div class="star-rating"> 
                                    <ul>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="star"><i class="fa fa-star" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                                <!-- End Star Rating -->
                            </div>
                            <!-- End Product Info -->
                            <!-- Start Prodcut Price Area -->
                            <div class="product-price-area"> 
                                <span class="price">
                                    <span class="amount">${{$product->lastPrice}}</span> <span><del>${{$product->startPrice}}</del></span>
                                </span>
                                <span class="add-to-cart"><a href="#"><i class="fa fa-plus" aria-hidden="true"></i>Add to Cart</a></span>
                            </div>
                            <!-- End Prodcut Price Area -->
                        </div>
                                <!-- End Single Prodcut -->
                            </div>
                        @endforeach   							
                        </div>
                        <!-- Start Product Paginations -->
                        <div class="prodcuts-pagination"> 
                            <div class="col-xs-12 col-sm-12 col-md-12"> 
                                <ul class="licuan-pagination">
                                    {{$products->links()}}
                                </ul>	
                            </div>					
                        </div>	
                        <!-- End Product Paginations -->						
                    </div>	
                    <!-- End Grid products -->
                
                    <!-- Start List products -->
                    <div id="list" class="tab-pane fade" role="tabpanel"> 
                        <!-- Start Products List View  -->
                        <div class="products-listview-inner"> 
                        @foreach($products as $product)
                            <div class="col-xs-12 col-sm-12 col-md-12">
                               <!-- Start Single Prodcut -->
                               <div class="single-product">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <!-- Start Product Thumbnail -->
                                            <div class="product-thumb-area">
                                                <!-- Start Product Image -->
                                                <div class="product-thumb">
                                                    <a href="#"><img height="356" width="294" src="{{$product->image}}" alt="product" /></a>
                                                    <span class="product-new">{{$product->user->name}}</span>
                                                </div>
                                                <!-- End Product Image -->
                                            </div>
                                            <!-- End Product Thumbnail -->
                                            
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8">
                                            <!-- Start Product Info -->
                                            <div class="product-short-info"> 
                                                <!-- Start product short description -->
                                                <p class="p-short-des"><a href="#">{{$product->name}}</a></p>
                                                <!-- End product short description -->
                                                <!-- Start Star Rating -->
                                                <div class="star-rating"> 
                                                    <ul>
                                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        <li class="star yes"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        <li class="star"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    </ul>
                                                </div>
                                                <!-- End Star Rating -->
                                                <div class="product-desc"> 
                                                    <p>{{$product->name}} owned and managed by {{$product->user->name}}</p>
                                                </div>
                                            </div>
                                            <!-- End Product Info -->
                                            <!-- Start Prodcut Price Area -->
                                            <div class="product-price-area"> 
                                                <span class="price">
                                                    <span class="amount">${{$product->lastPrice}}</span> <span><del>${{$product->startPrice}}</del></span>
                                                </span>
                                                <span class="add-to-cart">
                                                    <a href="#"><i class="fa fa-plus" aria-hidden="true"></i><span>Add to Cart</span></a>
                                                </span>
                                                <!-- Start Wish List  -->
                                                <span class="listview-wishlist"> 
                                                    <a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i> Wish List</a>
                                                    <a href="#"><i aria-hidden="true" class="fa fa-exchange"></i> Compare</a>
                                                </span>
                                                <!-- End Wish List  -->
                                            </div>
                                            <!-- Start Prodcut Price Area -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Start Single Prodcut -->
                            </div>
                        @endforeach
                        </div>
                        <!-- End Product List View -->
                        <!-- Start Pagination -->
                        <div class="licuan-pagination-area"> 
                            <div class="col-md-12"> 
                                <ul class="licuan-pagination">
                                    {{$products->links()}}
                                </ul>	
                            </div>					
                        </div>							  
                        <!-- End Pagination -->
                    </div>
                    <!-- End List products -->
                @endif
                </div>	
                <!-- End Tab Content -->
            </div>	
        </div>
    </div>
    <!-- End Main Content -->
    
@endsection