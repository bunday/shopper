-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 19, 2018 at 08:26 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `shopper`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `startPrice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastPrice` float NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `size`, `color`, `image`, `startPrice`, `lastPrice`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Vanessa Bings', '40', 'Green', '/uploads/1526547916.jpg', '179.99', 165.99, '1', '2018-05-17 08:05:17', '2018-05-17 08:05:17'),
(3, 'Venzi Boots', '32', 'Blue', '/uploads/1526584022.jpg', '39.99', 35.99, '2', '2018-05-17 18:07:03', '2018-05-17 18:07:03'),
(4, 'Sleek Vanz', '41', 'Brown', '/uploads/1526584077.jpg', '35.99', 31.99, '2', '2018-05-17 18:07:58', '2018-05-17 18:07:58'),
(5, 'Vans Closet', '40', 'Green', '/uploads/1526584170.jpg', '49.99', 36.99, '1', '2018-05-17 18:09:31', '2018-05-17 18:09:31'),
(6, 'Venzis Boots', '32', 'Blue', '/uploads/1526584022.jpg', '39.99', 35.99, '1', '2018-05-17 18:40:03', '2018-05-17 18:40:03'),
(7, 'Closet Boots', '35', 'Grey', '/uploads/1526584170.jpg', '29.99', 26.99, '2', '2018-05-17 19:09:31', '2018-05-17 19:09:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;