<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $new = App\Product::latest()->get()->take(5);
    $random = App\Product::inRandomOrder()->get()->take(5);
    return view('landing')->with(compact('new','random'));
});

Route::get('/explore', function(){
    $products = App\Product::latest()->paginate(4);
    return view('product')->with(compact('products'));
});

Route::post('/product-filter',function(Illuminate\Http\Request $r){
    $query = $r->productkeyword;
    $products = App\Product::where('name','LIKE','%'.$query.'%')->orderBy('lastPrice','asc')->paginate(4);
    return view('product')->with(compact('products'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/product/add', 'HomeController@addNewProduct');
Route::post('/product/update', 'HomeController@updateProduct');
Route::post('/product/delete', 'HomeController@deleteProduct');