<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Product;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Auth::user()->products()->get();
        return view('dashboard')->with(compact('products'));
    }

    public function addNewProduct(Request $r)
    {
        $product = new Product();
        $product->name = $r->productName;
        $product->color = $r->productColor;
        $product->size = $r->productSize;
        $product->startPrice = $r->productStartPrice;
        $product->lastPrice = $r->productLastPrice;
        $product->user_id = Auth::user()->id;

        $image = $r->productImage;
        if(!is_null($image)){
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save( public_path('/uploads/' . $filename));
            $product->image = '/uploads/'.$filename;
        }

        $product->save();
        return redirect('/home');
    }
    public function updateProduct(Request $r)
    {
        $product = Product::find($r->productId);
        $product->name = $r->productName;
        $product->color = $r->productColor;
        $product->size = $r->productSize;
        $product->startPrice = $r->productStartPrice;
        $product->lastPrice = $r->productLastPrice;
        
        $image = $r->productImage;
        if(!is_null($image)){
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save( public_path('/uploads/' . $filename));
            $product->image = '/uploads/'.$filename;
        }

        $product->save();
        return redirect('/home');
    }
    public function deleteProduct(Request $r){
        $product = Product::find($r->productId);
        $product->delete();
        return redirect('/home');
    }
}
